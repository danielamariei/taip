package api.suite;

import java.net.URL;
import java.util.List;

import model.Artist;
import model.Genre;

public class Recommendation {
	String title;
	List<Artist> artists;
	Genre genre;
	URL recommendationURL;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Artist> getArtists() {
		return artists;
	}

	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public URL getRecommendationURL() {
		return recommendationURL;
	}

	public void setRecommendationURL(URL recommendationURL) {
		this.recommendationURL = recommendationURL;
	}

}