package model;

import java.io.File;
import java.util.List;

public class SingletonTrackIndexing {
    private List<Hallmark> hallmarks;

    public void init(File file) {};
    public void init(File[] files) {}
    
	public List<Hallmark> getHallmarks() {
		return hallmarks;
	}
	public void setHallmarks(List<Hallmark> hallmarks) {
		this.hallmarks = hallmarks;
	};
}

