package model;

public enum Genre {
	HIP_HOP, HOUSE, CLASSICAL, FOLK, COUNTRY, POP
}
