package interfaces;


public interface AbstractHallmarkFactory {
	HallmarkCreator getHallmarkFactory();
}