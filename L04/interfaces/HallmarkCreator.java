package interfaces;


public interface HallmarkCreator {
	void createHallmark(Track track);
}