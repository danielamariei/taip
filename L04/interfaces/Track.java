package interfaces;


public interface Track extends Media {
    int getLengthInSeconds();
    int getBitrate();
}
