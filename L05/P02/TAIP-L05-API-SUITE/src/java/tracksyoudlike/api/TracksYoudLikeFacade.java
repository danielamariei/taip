/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tracksyoudlike.api;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author Amariei G. Daniel
 */
@Path("tracksYoudLike")
public class TracksYoudLikeFacade {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TracksYoudLikeFacade
     */
    public TracksYoudLikeFacade() {
    }

    /**
     * Retrieves representation of an instance of tracksyoudlike.api.TracksYoudLikeFacade
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("*/*")
    public String getRecomendations() {
        return ("{\"recommendations\" : {\"recommendation\" : {\"artist\": \"Michael Jackson\"}}}");
    }

}
