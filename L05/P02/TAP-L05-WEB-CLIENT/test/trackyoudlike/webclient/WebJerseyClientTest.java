/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trackyoudlike.webclient;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Amariei G. Daniel
 */
public class WebJerseyClientTest {
    
    public WebJerseyClientTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRecomendations method, of class WebJerseyClient.
     */
    @Test
    public void testGetRecomendations() {
        System.out.println("getRecomendations");
        WebJerseyClient instance = new WebJerseyClient();
     
        String result = instance.getRecomendations();
        assertNotNull(result);
    }

    /**
     * Test of close method, of class WebJerseyClient.
     */
    @Test
    public void testClose() {
        System.out.println("close");
        WebJerseyClient instance = new WebJerseyClient();
        instance.close();
    }
}