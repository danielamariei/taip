/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trackyoudlike.webclient;

import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Amariei G. Daniel
 */
public class WebJerseyClient2Test {
    
    public WebJerseyClient2Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

 

    /**
     * Test of getRecommendationsAsMap method, of class WebJerseyClient.
     */
    @Test
    public void testGetRecommendationsAsMap() throws Exception {
        System.out.println("getRecommendationsAsMap");
        WebJerseyClient instance = new WebJerseyClient();
        Map expResult = null;
        Map result = instance.getRecommendationsAsMap();
        assertNotNull(result);
    }

    /**
     * Test of close method, of class WebJerseyClient.
     */
    @Test
    public void testClose() {
        System.out.println("close");
        WebJerseyClient instance = new WebJerseyClient();
        instance.close();
    }
}