/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trackyoudlike.webclient;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Jersey REST client generated for REST resource:TracksYoudLikeFacade
 * [tracksYoudLike]<br>
 * USAGE:
 * <pre>
 *        WebJerseyClient client = new WebJerseyClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author Amariei G. Daniel
 */
public class WebJerseyClient {
    private static ObjectMapper mapper = new ObjectMapper();

    private WebResource webResource;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/TAIP-L05-API-SUITE/webresources";

    public WebJerseyClient() {
        com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = Client.create(config);
        webResource = client.resource(BASE_URI).path("tracksYoudLike");
    }

    public String getRecomendations() throws UniformInterfaceException {
        WebResource resource = webResource;
        return resource.accept(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }

    public Map<String, Object> getRecommendationsAsMap() throws UniformInterfaceException, IOException {
        WebResource resource = webResource;
        String data =
                resource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(String.class);
        
        Map<String,Object> recommendations = mapper.readValue(data, Map.class);
        return recommendations;
    }

    public void close() {
        client.destroy();
    }
}
