/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services.rest;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author Amariei G. Daniel
 */
public class TrackRecommendationsRequestHandler extends RecommendationsRequestHandler {

    public TrackRecommendationsRequestHandler(RecommendationsRequestHandler successor) {
        super(successor);
    }

    /**
     *
     * @param request
     * @param response
     */
    @Override
    public void handleRecommendationsRequest(MultivaluedMap<String, String> request, Recommendations response) {
        response.add("recomandari 2", "{lista de recomandari}");
        super.invokeNextHandlerInChain(request, response);
    }
}
