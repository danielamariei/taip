/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services.rest;

import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author Amariei G. Daniel
 */
public class RequestHandler {
    private static RecommendationsRequestHandler recommendationHandlersChain;
    
    static {
        // initialize the chain
        recommendationHandlersChain = new TrackRecommendationsRequestHandler(new ArtistRecommendationsRequestHandler(null));
    }
    
    public Recommendations handleRequest(MultivaluedMap<String, String> request, Recommendations response) {
        recommendationHandlersChain.handleRecommendationsRequest(request, response);
        return null;
    }
}
