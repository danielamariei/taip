/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services.rest;

import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author Amariei G. Daniel
 */
abstract class RecommendationsRequestHandler {
    // the next handler in the chain of resposibilities
    private RecommendationsRequestHandler successor;
    
    public RecommendationsRequestHandler(RecommendationsRequestHandler successor) {
        this.successor = successor;
    }
    
    abstract void handleRecommendationsRequest(MultivaluedMap<String, String> request, Recommendations response);
    
    // call the next handler in the chain
    public void invokeNextHandlerInChain(MultivaluedMap<String, String> request, Recommendations response) {
        if (successor != null) {
            successor.handleRecommendationsRequest(request, response);
        }
    }
}
