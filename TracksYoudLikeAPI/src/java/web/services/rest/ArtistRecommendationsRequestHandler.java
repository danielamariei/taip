/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services.rest;

import de.umass.lastfm.Artist;
import de.umass.lastfm.Caller;
import de.umass.lastfm.User;
import java.util.Collection;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author Amariei G. Daniel
 */
public class ArtistRecommendationsRequestHandler extends RecommendationsRequestHandler {

    private static String KEY = "2226933931f01f4699124db4aa54bf3c";
    private static String REQUEST_KEY = "artists";

    public ArtistRecommendationsRequestHandler(RecommendationsRequestHandler successor) {
        super(successor);
    }

    /**
     *
     * @param request
     * @param response
     */
    @Override
    public void handleRecommendationsRequest(MultivaluedMap<String, String> request, Recommendations response) {
        if (request.containsKey(REQUEST_KEY) && "true".equals(request.getFirst(REQUEST_KEY))) {
            String user = request.getFirst("account");
            
            Caller.getInstance().setUserAgent("tst");
            Collection<Artist> artists = User.getTopArtists(user, KEY);
            
            response.add("artists", artists);
        }
        super.invokeNextHandlerInChain(request, response);
    }
}
