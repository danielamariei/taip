/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services.rest;

import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Amariei G. Daniel
 */
public class Recommendations {
    private Map<String, List<String>> recommendations;
            
    public Recommendations() {
        recommendations = new TreeMap<>();
    }
            
            
    public Map<? extends Object, ? extends Object> getAsMap() {
        return constructEnclosingHashMap();
    } 
    
    public String getAsJSON() {
        Gson gson = new Gson();
        return gson.toJson(constructEnclosingHashMap());
    }
    
    public void add(String key, List<String> value) {
        recommendations.put(key, value);
    }
    
    private Map<String, List<String>> constructEnclosingHashMap() {
        Map<String, List<String>> result = new TreeMap<>();
        result.put("recommendations", recommendations);
        return result;
    }
}
