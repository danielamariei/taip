/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services.rest;

import com.google.gson.Gson;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;
import javax.management.Query;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Request;

/**
 * REST Web Service
 *
 * @author Amariei G. Daniel
 */
@Path("getRecommendations")
public class GetRecommendationsResource {

    private static RequestHandler requestHandler;

    static {
        requestHandler = new RequestHandler();
    }
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GetRecommendationsResource
     */
    public GetRecommendationsResource() {
    }

    /**
     * Retrieves representation of an instance of
     * web.services.rest.GetRecommendationsResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getRecommendations() {
        MultivaluedMap<String, String> request = context.getQueryParameters();
        Recommendations recommendations = new Recommendations();

        requestHandler.handleRequest(request, recommendations);

        return recommendations.getAsJSON();
    }
}
